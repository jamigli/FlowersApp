﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowersApp.Models
{
    public class Flower
    {
        public int FlowerId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public override int GetHashCode()
        {
            return FlowerId;
        }
    }
}
