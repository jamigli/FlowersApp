﻿using FlowersApp.Services;
using System.Collections.ObjectModel;
using System;
using FlowersApp.Models;
using System.Collections.Generic;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Linq;
using System.ComponentModel;

namespace FlowersApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Atributes
        private ApiService apiService;
        private NavigationService navigationService;
        private DialogService dialogService;
        private bool isRefreshing;
        #endregion

        #region Properties
        public ObservableCollection<FlowerItemViewModel> Flowers { get; set; }
        public NewFlowerViewModel NewFlower { get; set; }
        public EditFlowerViewModel EditFlower { get; set; }

        public bool IsRefreshing
        {
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRefreshing"));
                }
            }
            get
            {
                return isRefreshing;
            }
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;

            apiService = new ApiService();
            navigationService = new NavigationService();
            dialogService = new DialogService();

            Flowers = new ObservableCollection<FlowerItemViewModel>();
        }
        #endregion

        #region Methods
        private async void LoadFlowers()
        {
            var response = await apiService.Get<Flower>("http://flowersback2.azurewebsites.net", "/api", "/Flowers");
            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }
            RealoadFlowers((List<Flower>)response.Result);
        }
        private void RealoadFlowers(List<Flower> flowers)
        {
            Flowers.Clear();
            foreach (var flower in flowers.OrderBy(ord => ord.Description))
            {
                Flowers.Add(new FlowerItemViewModel
                {
                    Description = flower.Description,
                    FlowerId = flower.FlowerId,
                    Price = flower.Price,
                });
            }
        }
        #endregion

        #region Commands
        public ICommand AddFlowerCommand { get { return new RelayCommand(AddFlower); } }
        public ICommand RefreshFlowersCommand { get { return new RelayCommand(RefreshFlowers); } }

        private void RefreshFlowers()
        {
            IsRefreshing = true;
            LoadFlowers();
            IsRefreshing = false;
        }

        private async void AddFlower()
        {
            await navigationService.Navigate("NewFlowerPage");
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new MainViewModel();
            }
            return instance;
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
