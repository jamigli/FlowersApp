﻿using FlowersApp.Models;
using FlowersApp.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlowersApp.ViewModels
{
    public class EditFlowerViewModel : Flower, INotifyPropertyChanged
    {
        private DialogService dialogService;
        private bool isRunning;
        private string description;
        private decimal price;
        private bool isEnabled;
        private ApiService apiService;
        private NavigationService navigationService;

        public event PropertyChangedEventHandler PropertyChanged;

        public EditFlowerViewModel(Flower flower)
        {
            Description = flower.Description;
            Price = flower.Price;
            FlowerId = flower.FlowerId;
            dialogService = new DialogService();
            apiService = new ApiService();
            IsEnabled = true;
            navigationService = new NavigationService();
        }

        public new string Description
        {
            set
            {
                if (description != value)
                {
                    description = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Description"));
                }
            }
            get
            {
                return description;
            }
        }
        public new decimal Price
        {
            set
            {
                if (price != value)
                {
                    price = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Price"));
                }
            }
            get
            {
                return price;
            }
        }

        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }
        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        public ICommand SaveFlowerCommand { get { return new RelayCommand(SaveFlower); } }

        private async void SaveFlower()
        {
            if (string.IsNullOrEmpty(Description))
            {
                await dialogService.ShowMessage("Error", "You must enter a description");
                return;
            }
            if (Price <= 0)
            {
                await dialogService.ShowMessage("Error", "You must enter a grather than zero in price");
                return;
            }
            IsRunning = true;
            IsEnabled = false;
            var response = await apiService.Put("http://flowersback2.azurewebsites.net", "/api", "/Flowers", this);
            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }

        public ICommand DeleteFlowerCommand { get { return new RelayCommand(DeleteFlower); } }

        private async void DeleteFlower()
        {
            var answer = await dialogService.ShowConfirm("Confirm", "Are u sure to delete this?");
            if (!answer)
            {
                return;
            }
            IsRunning = true;
            IsEnabled = false;
            var response = await apiService.Delete("http://flowersback2.azurewebsites.net", "/api", "/Flowers", this);
            IsRunning = false;
            IsEnabled = true;
            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }
    }
}
