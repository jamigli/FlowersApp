﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlowersApp.Models;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using FlowersApp.Services;

namespace FlowersApp.ViewModels
{
    public class FlowerItemViewModel : Flower
    {
        private NavigationService navigationService;
        public FlowerItemViewModel()
        {
            navigationService = new NavigationService();
        }
        public ICommand EditFlowerCommand { get { return new RelayCommand(EditFlower); } }
        private async void EditFlower()
        {
            await navigationService.EditFlower(this);
        }
    }
}
